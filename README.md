Payment-System: Stores all the transcation made in an account. 
Calculates daily and monthly interest.
keep the historical data for the accounts.




Technologies used:

1. Spring boot
2. ActiveMq
3. MongoDb 
4. Swagger


Key points of implementation:

1. Daily interest history and monthly interest history is maintained.
2. Timezone related edge cases are considered. all the dates are stored as per GMT. 
3. Application can run on any server in any local timezone. dates will be adjusted logically.
4. dates have been precisely handled.
5. asynch /reactive approach used as per applicability.
6. Account active/inactive status is taken care.
7. Seperation of concerns between layer exist.
8. Code is easily testable.
9. Code readability/maintainability is given importance.
10. Monthly interest rate assumed is 5.25.
11. Duplicate request handled appropriately. (both for account creation and eod interest)
12. Provided swagger doc and ui support.
13. Lot of history data have to be stored Nosql document db is selected over any other.


Approach for solution: there would be new resoursce created for each account on the first request for eod interest calculation. post that the resource would get updated for each day interest. the same object gets updated with monthly interest by summing all the interest present in that object. any account monthly and yearly history can be feteched any time.



here is the swagger end point http://localhost:8081/swagger-ui.html#/Payment-System-apis


There are three api's in the Payment system 



1. /payment-system/accounts - this api creates an account with the provided account number. the account number has to be unique irrespective of branch code. all the accounts are by default created in active state.

Stores the document in Account collection- 

Sample Account Collection document:

{
"_id":1,
"branchId":"axis",
"date":{
"$date":"2021-11-30T18:30:00.000Z"
},
"isActive":true,
"_class":"com.system.payment.model.Account"
}





2. /payment-system/computeEodInterest - this api publishes the account data to the eod interest compuation queue. Subscriber reads all the data . a new entry in the database happens for the first request of the month for all the active account. post that for the entire month same resource gets updated.all the history is getting maintained.

Crates or update the dcument in Account_Interest_History

Sample Account_Interest_History Collection Document:

{
"_id":{
"$oid":"61d3249dd651ad2a85bdc71f"
},
"accountId":1,
"year":2021,
"month":12,
"interestData":[
{
"date":{
"$date":"2021-12-01T18:30:00.000Z"
},
"interest":"5.25"
},
{
"date":{
"$date":"2021-12-02T18:30:00.000Z"
},
"interest":"15.75"
},
{
"date":{
"$date":"2021-12-03T18:30:00.000Z"
},
"interest":"15.75"
},
{
"date":{
"$date":"2021-12-04T18:30:00.000Z"
},
"interest":"15.75"
}
],
"_class":"com.system.payment.model.AccountInterestHistory"
}


3. /payment-system/monthlyInterest/{month}?year=2021 - this api publishes the data for end of the month interest compuataion for all the accounts to end of the month interest computataion  quque . subscriber process the data and updates the cummulative interest for the month.

always updates the record in Account_Interest_History

Sample Account_Interest_History Collection Document with Cumulative interest of the month ("totalInterestForTheMonth":"52.50",)




{
"_id":{
"$oid":"61d3249dd651ad2a85bdc71f"
},
"accountId":1,
"year":2021,
"month":12,
"interestData":[
{
"date":{
"$date":"2021-12-01T18:30:00.000Z"
},
"interest":"5.25"
},
{
"date":{
"$date":"2021-12-02T18:30:00.000Z"
},
"interest":"15.75"
},
{
"date":{
"$date":"2021-12-03T18:30:00.000Z"
},
"interest":"15.75"
},
{
"date":{
"$date":"2021-12-04T18:30:00.000Z"
},
"interest":"15.75"
}
],
"totalInterestForTheMonth":"52.50",
"_class":"com.system.payment.model.AccountInterestHistory"
}


How to run the project :

Import as maven project in any IDE and run the main class or run maven pacakge from root of the project and then run the jar file.

Note: update the mongo db property in the application.properties file before running the project.

could not provide more junit coverage due to time shortage.