package com.system.payment.service;

import com.system.payment.dto.AccountData;
import com.system.payment.exceptions.AccountAlreadyExist;
import com.system.payment.model.Account;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.LocalDate;

import static org.mockito.Mockito.when;


public class AccountServiceTestForAccountCreation {


    MongoDataService mongoDataService = Mockito.mock(MongoDataService.class);
    PublishMessageService publishMessageService = Mockito.mock(PublishMessageService.class);


    @Test
    public void Test_Account_Creation_when_no_existing_account_with_provided_id() throws AccountAlreadyExist {
        LocalDate date = LocalDate.now();
        AccountService accountService = new AccountService(mongoDataService, publishMessageService);
        AccountData accountData = new AccountData();
        accountData.setAccountNumber(123);
        accountData.setDate(date);
        accountData.setBranchId("sbi");
        Account account = new Account();
        account.setAccountNumber(accountData.getAccountNumber());
        when(mongoDataService.isAccountExistByAccountId(123)).thenReturn(false);
        when(mongoDataService.saveAccount(Mockito.any())).thenReturn(account);
        Account createdAccount = accountService.createAccount(accountData);
        Assertions.assertEquals(123, createdAccount.getAccountNumber());


    }

    @Test
    public void Test_Account_Creation_when_account_exist_with_provided_id() {
        LocalDate date = LocalDate.now();
        AccountService accountService = new AccountService(mongoDataService, publishMessageService);
        AccountData accountData = new AccountData();
        accountData.setAccountNumber(123);
        accountData.setDate(date);
        accountData.setBranchId("sbi");
        when(mongoDataService.isAccountExistByAccountId(123)).thenReturn(true);

        try {
            accountService.createAccount(accountData);
        } catch (AccountAlreadyExist e) {
            Assertions.assertTrue(e.toString().contains("the account with the provided id already exist"));
        }


    }

}









