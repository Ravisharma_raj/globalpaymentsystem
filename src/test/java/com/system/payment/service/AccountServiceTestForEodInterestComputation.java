package com.system.payment.service;

import com.system.payment.dto.EodAccountBalanceData;
import com.system.payment.model.Account;
import com.system.payment.model.AccountInterestHistory;
import com.system.payment.model.InterestData;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.when;

public class AccountServiceTestForEodInterestComputation {

    MongoDataService mongoDataService = Mockito.mock(MongoDataService.class);
    PublishMessageService publishMessageService = Mockito.mock(PublishMessageService.class);


    @Test
    public void Test_To_Compute_EOD_Interest_For_Accounts_For_The_First_Time() throws Exception {

        Account account = new Account();
        account.setAccountNumber(123);
        account.setDate(LocalDateTime.of(2021,01,01,00,00));

        Optional<Account> optionalAccount = Optional.of(account);
        AccountInterestHistory accountInterestHistory = new AccountInterestHistory();
        Optional<AccountInterestHistory> optionalAccountInterestHistory = Optional.empty();


        when(mongoDataService.isAccountExistByAccountId(123)).thenReturn(true);
        when(mongoDataService.findActiveAccountById(123)).thenReturn(optionalAccount);
        when(mongoDataService.getAccountHistoryForProvidedMonthAndYear(123,2021,2)).thenReturn(optionalAccountInterestHistory);
        when(mongoDataService.findById(123)).thenReturn(Optional.of(account));
        AccountService accountService = new AccountService(mongoDataService, publishMessageService);
        EodAccountBalanceData eodAccountBalanceData = new EodAccountBalanceData();
        EodAccountBalanceData.AccountBalance accountBalance = new EodAccountBalanceData.AccountBalance();
        accountBalance.setBalance(500);
        accountBalance.setBsb("axis");
        accountBalance.setIdentification(123);
        List<EodAccountBalanceData.AccountBalance> accountBalances = Arrays.asList(accountBalance);
        eodAccountBalanceData.setData(accountBalances);
        eodAccountBalanceData.setBalanceDate(LocalDate.of(2021,02,01));
        accountService.computeEodInterestForAccounts(eodAccountBalanceData);


    }

    @Test
    public void Test_Compute_EOD_Interest_For_Accounts_After_First_Time() throws Exception {

        InterestData interestData1 = new InterestData();
        interestData1.setInterest(new BigDecimal(20.05));
        interestData1.setDate(LocalDateTime.of(2021,02,05,01,01));
        InterestData interestData2 = new InterestData();
        interestData2.setInterest(new BigDecimal(20.05));
        interestData2.setDate(LocalDateTime.of(2021,03,06,01,01));
        List<InterestData> interestDataList = new ArrayList<>();
        interestDataList.add(interestData1);
        interestDataList.add(interestData2);

        Account account = new Account();
        account.setAccountNumber(123);
        account.setDate(LocalDateTime.of(2021,01,01,00,00));

        Optional<Account> optionalAccount = Optional.of(account);
        AccountInterestHistory accountInterestHistory = new AccountInterestHistory();

        accountInterestHistory.setInterestData(interestDataList);
        Optional<AccountInterestHistory> optionalAccountInterestHistory = Optional.of(accountInterestHistory);


        when(mongoDataService.isAccountExistByAccountId(123)).thenReturn(true);
        when(mongoDataService.findActiveAccountById(123)).thenReturn(optionalAccount);
        when(mongoDataService.getAccountHistoryForProvidedMonthAndYear(123,2021,2)).thenReturn(optionalAccountInterestHistory);
        when(mongoDataService.saveInterestHistory(optionalAccountInterestHistory.get())).thenReturn(accountInterestHistory);
        when(mongoDataService.findById(123)).thenReturn(Optional.of(account));
        AccountService accountService = new AccountService(mongoDataService, publishMessageService);
        EodAccountBalanceData eodAccountBalanceData = new EodAccountBalanceData();
        EodAccountBalanceData.AccountBalance accountBalance = new EodAccountBalanceData.AccountBalance();
        accountBalance.setBalance(500);
        accountBalance.setBsb("axis");
        accountBalance.setIdentification(123);
        List<EodAccountBalanceData.AccountBalance> accountBalances = Arrays.asList(accountBalance);
        eodAccountBalanceData.setData(accountBalances);
        eodAccountBalanceData.setBalanceDate(LocalDate.of(2021,02,01));

        accountService.computeEodInterestForAccounts(eodAccountBalanceData);

        Assertions.assertEquals(interestDataList.get(2).getInterest() , new BigDecimal(26.25));

    }
}
