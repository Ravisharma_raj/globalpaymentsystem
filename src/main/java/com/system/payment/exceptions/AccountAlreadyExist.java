package com.system.payment.exceptions;

public class AccountAlreadyExist extends Exception {
    @Override
    public String toString() {
        return "the account with the provided id already exist";
    }
}
