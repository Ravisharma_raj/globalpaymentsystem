package com.system.payment.controllers;

import com.system.payment.dto.AccountData;
import com.system.payment.dto.EodAccountBalanceData;
import com.system.payment.exceptions.AccountAlreadyExist;
import com.system.payment.service.AccountService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/payment-system")
@Api(tags={"Payment-System-Apis"})
public class EntryPoints {


    private final AccountService accountService;

    @Autowired
    public EntryPoints(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping()
    public String healthCheck() {
        return "running fine";
    }


    @PostMapping("/accounts")
    public ResponseEntity<AccountData> createAccount(@Validated @RequestBody AccountData accountData) throws AccountAlreadyExist {
        accountService.createAccount(accountData);
        return new ResponseEntity<AccountData>(accountData, HttpStatus.CREATED);

    }

    @PostMapping("/computeEodInterest")
    public ResponseEntity calculateDailyInterest(@Validated @RequestBody EodAccountBalanceData eodAccountBalanceData) throws Exception {
        accountService.publishEodInterestMessage(eodAccountBalanceData);
        return new ResponseEntity(HttpStatus.CREATED);

    }

    @PutMapping("/monthlyInterest/{month}")
    public ResponseEntity<String> calculateMonthlyInterestForAllAccount(@PathVariable Integer month, @RequestParam(required = false) Integer year) throws Exception {
        accountService.sumOfInterestTillDateForTheMonth(month , year);
        return new ResponseEntity<>("Request Accepted" , HttpStatus.ACCEPTED);
    }


}
