package com.system.payment.controllers;

import com.system.payment.dto.CustomExceptionMessage;
import com.system.payment.exceptions.AccountAlreadyExist;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ControllerExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(AccountAlreadyExist.class)
    public ResponseEntity<CustomExceptionMessage> error(AccountAlreadyExist ex, WebRequest request) {
        CustomExceptionMessage customExceptionMessage = new CustomExceptionMessage();
        customExceptionMessage.setErrorMessage(ex.toString());
        customExceptionMessage.setErrorCode(400);
        return new ResponseEntity<>(customExceptionMessage, HttpStatus.BAD_REQUEST);


    }


}
