package com.system.payment.model;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;

@Data

public class InterestData {

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    LocalDateTime date;
    BigDecimal interest;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InterestData interestData = (InterestData) o;
        return this.getDate().isEqual(interestData.getDate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(date, interest);
    }
}
