package com.system.payment.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Document("Account")
@Data
public class Account {
   String branchId;
   @Id
   Integer accountNumber;
   LocalDateTime date;
   Boolean isActive = true;

}
