package com.system.payment.model;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;
import java.util.List;

@Document("Account_Interest_History")
@Data
public class AccountInterestHistory {
    String _id;
    Integer accountId;
    Integer year;
    Integer month;
    List<InterestData> interestData;
    BigDecimal totalInterestForTheMonth;


}
