package com.system.payment;

import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.jms.annotation.EnableJms;

import javax.jms.Queue;

@SpringBootApplication
@EnableMongoRepositories
@EnableJms

public class GlobalPaymentSystem {

    public static void main(String[] args) {
        SpringApplication.run(GlobalPaymentSystem.class, args);
    }

    @Bean
    public Queue eodInterestQueue() {
        return new ActiveMQQueue("EodInterestQueue");
    }

    @Bean
    public Queue endOfTheMonthInterestQueue() {
        return new ActiveMQQueue("EndOfTheMonthInterestQueue");
    }

}
