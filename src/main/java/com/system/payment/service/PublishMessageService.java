package com.system.payment.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.stereotype.Component;

import javax.jms.Queue;

@Component
public class PublishMessageService {

    @Autowired
    private JmsMessagingTemplate jmsMessagingTemplate;

    @Autowired
    @Qualifier("eodInterestQueue")
    private Queue eodInterestQueue;

    @Autowired
    @Qualifier("endOfTheMonthInterestQueue")
    private Queue endOfMonthInterestQueue;

    public void publishEodInterestMessage(String msg) throws Exception {
        this.jmsMessagingTemplate.convertAndSend(this.eodInterestQueue, msg);
        System.out.println("Message has been put to queue 1 by sender");
    }

    public void publishEndOfTheMonthInterestMessage( String msg) throws Exception {
        this.jmsMessagingTemplate.convertAndSend(this.endOfMonthInterestQueue, msg);
        System.out.println("Message has been put to queue 2 by sender");
    }


}
