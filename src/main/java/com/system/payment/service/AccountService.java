package com.system.payment.service;

import com.google.gson.Gson;
import com.system.payment.dto.AccountData;
import com.system.payment.dto.EndOfMonthInterestData;
import com.system.payment.dto.EodAccountBalanceData;
import com.system.payment.exceptions.AccountAlreadyExist;
import com.system.payment.model.Account;
import com.system.payment.model.AccountInterestHistory;
import com.system.payment.model.InterestData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class AccountService {

    @Value("${default.year}")
    Integer defaultYear;


    private final MongoDataService mongoDataService;
    private final PublishMessageService publishMessage;

    @Autowired
    public AccountService(MongoDataService mongoDataService, PublishMessageService publishMessage) {

        this.mongoDataService = mongoDataService;
        this.publishMessage = publishMessage;
    }

    public Account createAccount(AccountData accountData) throws AccountAlreadyExist {
        Account account = new Account();
        account.setAccountNumber(accountData.getAccountNumber());
        account.setBranchId(accountData.getBranchId());
        int offset = getOffsetForGMTConversion(accountData.getDate());
        LocalDateTime localDateTimeInGMT= accountData.getDate().atStartOfDay().plusSeconds(offset);
        account.setDate(localDateTimeInGMT);
        if (mongoDataService.isAccountExistByAccountId(accountData.getAccountNumber())) {
            throw new AccountAlreadyExist();
        }
        return mongoDataService.saveAccount(account);

    }

    private int getOffsetForGMTConversion(LocalDate localDate) {
        LocalDateTime localDateTime = localDate.atStartOfDay();
        Calendar now = Calendar.getInstance();
        TimeZone timeZone = now.getTimeZone();
        ZonedDateTime zonedDateTime = ZonedDateTime.of(localDateTime,timeZone.toZoneId());
        return zonedDateTime.getOffset().getTotalSeconds();
    }

    public void computeEodInterestForAccounts(EodAccountBalanceData eodAccountBalanceData) throws Exception {

        List<EodAccountBalanceData.AccountBalance> balance = eodAccountBalanceData.getData();
        Integer year = eodAccountBalanceData.getBalanceDate().getYear();
        Integer month = eodAccountBalanceData.getBalanceDate().getMonth().getValue();
        Integer day = eodAccountBalanceData.getBalanceDate().getDayOfMonth();

        for (EodAccountBalanceData.AccountBalance accountBalanceData : balance) {
            List<InterestData> interestDataList = new ArrayList<>();
            InterestData interestData = new InterestData();
            AccountInterestHistory accountInterestHistory = new AccountInterestHistory();
            Integer accountId = accountBalanceData.getIdentification();
            if(!isAccountActive(accountId)){
                continue;
            }

            if(!isAccountOpeningDateBeforeTheEodInterestDate(accountId ,eodAccountBalanceData.getBalanceDate())){
                continue;

            }

            BigDecimal interest = calculateInterestForTheDay(accountBalanceData.getBalance());
            Optional<AccountInterestHistory> optionalAccountInterestHistory = mongoDataService.getAccountHistoryForProvidedMonthAndYear(accountId, year, month);
            if (optionalAccountInterestHistory.isPresent()) {
                AccountInterestHistory existinAaccountHistory = optionalAccountInterestHistory.get();
                updateExistingAccountHistory(existinAaccountHistory, interest, eodAccountBalanceData.getBalanceDate());
                mongoDataService.saveInterestHistory(existinAaccountHistory);
                continue;
            }

            int offset = getOffsetForGMTConversion(eodAccountBalanceData.getBalanceDate());
            LocalDateTime localDateTimeInGMT= eodAccountBalanceData.getBalanceDate().atStartOfDay().plusSeconds(offset);
            interestData.setInterest(interest);
            interestData.setDate(localDateTimeInGMT);
            interestDataList.add(interestData);
            accountInterestHistory.setAccountId(accountBalanceData.getIdentification());
            accountInterestHistory.setMonth(month);
            accountInterestHistory.setYear(year);
            accountInterestHistory.setInterestData(interestDataList);
            mongoDataService.saveInterestHistory(accountInterestHistory);

        }

    }

    private boolean isAccountActive(Integer accountId) {
        return mongoDataService.findActiveAccountById(accountId).isPresent();
    }

    private boolean isAccountOpeningDateBeforeTheEodInterestDate(Integer accountId, LocalDate balanceDate) {
        Optional<Account> optionalAccount=mongoDataService.findById(accountId);
        LocalDateTime ldt = LocalDateTime.of(balanceDate , LocalTime.of(0,0));
        if(optionalAccount.get().getDate().isBefore(ldt)){
            return true;
        }
        return false;
    }

    private void updateExistingAccountHistory(AccountInterestHistory existinAaccountHistory, BigDecimal interest, LocalDate balanceDate) {
        InterestData interestData = new InterestData();
        interestData.setInterest(interest);
        int offset = getOffsetForGMTConversion(balanceDate);
        LocalDateTime localDateTimeInGMT= balanceDate.atStartOfDay().plusSeconds(offset);
        interestData.setDate(localDateTimeInGMT);
        List<InterestData> interestDataLst = existinAaccountHistory.getInterestData();

        if (interestDataLst.stream().filter(i -> i.equals(interestData)).findFirst().isPresent()) {
            return;
        }

        interestDataLst.add(interestData);
    }

    public void calculateTotalInterestForTheMonth(int month, int year) {

        List<Account> accounts = mongoDataService.findAllAccount();

        List<Account> activeAccounts = accounts.stream().filter(acc -> acc.getIsActive() == true).collect(Collectors.toList());
        for (Account acc : activeAccounts) {

            cumalteAlldaysInterestForTheMonth(acc.getAccountNumber(), month, year);
        }

    }

    public void sumOfInterestTillDateForTheMonth(Integer month , Integer year) throws Exception {

        if (year == null) {
            year = defaultYear;
        }
        EndOfMonthInterestData endOfMonthInterestData = new EndOfMonthInterestData();
        endOfMonthInterestData.setMonth(month);
        endOfMonthInterestData.setYear(year);
        publishMessage.publishEndOfTheMonthInterestMessage(new Gson().toJson(endOfMonthInterestData));

    }

    private BigDecimal calculateInterestForTheDay(Integer balance) {
        BigDecimal bal = new BigDecimal(balance);
        BigDecimal constant = new BigDecimal(100);
        BigDecimal interestRate = new BigDecimal(5.25);
        return interestRate.multiply(bal.divide(constant));
    }


    private void cumalteAlldaysInterestForTheMonth(Integer accountNumber, int month, int year) {
        Optional<AccountInterestHistory> accountInterestHistory = mongoDataService.findByAccountIdAndYearAndMonth(accountNumber,year,month);

        if (accountInterestHistory.isPresent()) {
            List<InterestData> interestData = accountInterestHistory.get().getInterestData();
            BigDecimal totalInterestTilldate = interestData.stream().map(in -> in.getInterest()).reduce((i, a) -> i.add(a)).get();
            accountInterestHistory.get().setTotalInterestForTheMonth(totalInterestTilldate);
            mongoDataService.saveInterestHistory(accountInterestHistory.get());
        }

    }

    public void publishEodInterestMessage(EodAccountBalanceData eodAccountBalanceData) throws Exception {
        publishMessage.publishEodInterestMessage(new Gson().toJson(eodAccountBalanceData));
    }
}



