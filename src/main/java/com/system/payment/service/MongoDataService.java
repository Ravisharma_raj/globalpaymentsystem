package com.system.payment.service;

import com.system.payment.model.Account;
import com.system.payment.model.AccountInterestHistory;
import com.system.payment.repository.AccountInterestHistoryRepo;
import com.system.payment.repository.AccountRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Service
public class MongoDataService {

    private final AccountInterestHistoryRepo accountInterestHistoryRepo;
    private final AccountRepo accountRepo;

    @Autowired
    public MongoDataService(AccountInterestHistoryRepo accountInterestHistoryRepo, AccountRepo accountRepo) {
        this.accountInterestHistoryRepo = accountInterestHistoryRepo;
        this.accountRepo = accountRepo;
    }

    public Optional<AccountInterestHistory> getAccountHistoryForProvidedMonthAndYear(Integer accountId, Integer year, Integer month) {
        return accountInterestHistoryRepo.findByAccountIdAndYearAndMonth(accountId, year, month);
    }

    public boolean isAccountExistByAccountId(int accountId){
        return accountRepo.existsById(accountId);
    }

    public Account saveAccount(Account account) {
        return accountRepo.save(account);
    }

    public AccountInterestHistory saveInterestHistory(AccountInterestHistory existinAaccountHistory) {
        return accountInterestHistoryRepo.save(existinAaccountHistory);
    }

    public List<Account> findAllAccount() {
        return accountRepo.findAll();
    }

    public Optional<AccountInterestHistory> findByAccountIdAndYearAndMonth(Integer accountNumber, int year, int month) {
        return accountInterestHistoryRepo.findByAccountIdAndYearAndMonth(accountNumber,year,month);
    }

    public Optional<Account> findActiveAccountById(Integer accountId) {
        return accountRepo.findByAccountNumberAndIsActive(accountId , true);
    }


    public Optional<Account> findById(Integer accountId) {
        return accountRepo.findById(accountId);
    }
}
