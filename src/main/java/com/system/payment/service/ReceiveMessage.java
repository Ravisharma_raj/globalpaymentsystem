package com.system.payment.service;

import com.google.gson.Gson;
import com.system.payment.dto.EndOfMonthInterestData;
import com.system.payment.dto.EodAccountBalanceData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class ReceiveMessage {

    @Autowired
    AccountService accountService;

    @JmsListener(destination = "EodInterestQueue")
    public void receiveQueue(String text) throws Exception {
        Gson gson = new Gson();
        EodAccountBalanceData e1=gson.fromJson(text,
                EodAccountBalanceData.class);
        accountService.computeEodInterestForAccounts(e1);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Message Received: "+text);
    }

    @JmsListener(destination = "EndOfTheMonthInterestQueue")
    public void receiveQueue1(String text) {
        Gson gson = new Gson();
        EndOfMonthInterestData e1=gson.fromJson(text,
                EndOfMonthInterestData.class);
        accountService.calculateTotalInterestForTheMonth(e1.getMonth(),e1.getYear());
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Message Received: "+text);
    }

}
