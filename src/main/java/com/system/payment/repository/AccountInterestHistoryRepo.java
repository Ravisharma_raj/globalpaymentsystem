package com.system.payment.repository;

import com.system.payment.model.AccountInterestHistory;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface AccountInterestHistoryRepo extends MongoRepository<AccountInterestHistory,Integer> {

    AccountInterestHistory findByAccountIdAndYear(Integer accountId,Integer Year);
    AccountInterestHistory findByAccountId(String accountId);

    Optional<AccountInterestHistory> findByAccountIdAndYearAndMonth(Integer accountId, Integer year, Integer month);
}
