package com.system.payment.repository;

import com.system.payment.model.Account;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;


public interface AccountRepo extends MongoRepository<Account,Integer> {


  Optional<Account> findByAccountNumberAndIsActive(Integer accountId, boolean b);

   Optional<Account> findByAccountNumber(Integer accountId);


}
