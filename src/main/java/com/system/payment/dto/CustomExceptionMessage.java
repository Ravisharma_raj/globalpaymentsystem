package com.system.payment.dto;

import lombok.Data;

@Data
public class CustomExceptionMessage {

    String errorMessage;
    int errorCode;
}
