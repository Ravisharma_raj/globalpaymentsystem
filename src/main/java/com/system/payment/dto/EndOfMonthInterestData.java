package com.system.payment.dto;

import lombok.Data;

@Data
public class EndOfMonthInterestData {

    Integer month;
    Integer year;

}
