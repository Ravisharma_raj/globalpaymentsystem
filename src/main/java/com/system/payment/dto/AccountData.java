package com.system.payment.dto;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;


@Data
public class AccountData {
    @NotNull
    @JsonAlias("bsb")
    String branchId;
    @NotNull
    @JsonAlias("identification")
    Integer accountNumber;
    @NotNull
    @JsonAlias("openingDate")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    LocalDate date;

}
