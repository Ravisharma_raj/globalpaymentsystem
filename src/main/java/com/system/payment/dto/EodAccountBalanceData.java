package com.system.payment.dto;

import lombok.Data;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.List;

@Data
@ToString

public class EodAccountBalanceData {

    @Data
   public static class AccountBalance {
        String bsb;
        Integer identification;
        Integer balance;

    }

    List<AccountBalance> data;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    LocalDate balanceDate;


}
